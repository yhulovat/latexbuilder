import os
from LatexBuilder import LatexBuilder


report = LatexBuilder(
    author = 'John Doe',
    title = 'Network images',
    graphicspath= ['../images/'],
    latex_template_file = './templates/report.tex',
    makefile_template_file = './templates/latex_makefile'
    )

dirs = ['fixed_circular_layout','optimal_layout']

for dir in dirs:

    report.addSection(dir.replace('_',' ').capitalize())

    images = []
    captions = []
    i=1
    j=1
    for f in os.listdir('./images/' + dir):
        if not '.png' in f:
            continue
        images.append(dir +'/' + f)
        captions.append(f[:-4].replace('_',' - '))

    report.addSection('All', level = 1)
    report.addGroupFigure('Networks',
                          image_list = images,
                          image_caption_list = captions,
                          image_label_suffix = 'all_%s' % ('circular' if 'circular' in dir else 'optimal'),
    )

    report.addSection('By class',level = 1)
    for i in xrange(0,2):
        report.addGroupFigure('Networks of class %s' % (i+1),
                              image_list = images[i*6:],
                              image_caption_list = captions[i*6:],
                              column_count = 3,
                              image_label_suffix = 'class_%s_%s' % ('circular' if 'circular' in dir else 'optimal', i),
        )

report.compile('./', 'report', pdflatex=True)